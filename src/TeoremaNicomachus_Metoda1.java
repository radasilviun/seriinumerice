public class TeoremaNicomachus_Metoda1 {

    public static int kthgroupsum(int k) {
        int cur = (k * (k - 1)) + 1;
        int sum = 0;

        while (k-- > 0) {
            sum += cur;
            cur += 2;
        }

        return sum;
    }

    public static void main(String[] args) {
        int k = 3;
        System.out.print(kthgroupsum(k));
    }
}
