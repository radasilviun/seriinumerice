public class SeriaPadovan {


    public static void main(String[] args) {
        int n = 12;
        System.out.println(pad(n));
    }

    static int pad(int n) {
        int pPrevPrev = 1, pPrev = 1,
                pCurr = 1, pNext = 1;

        for (int i = 3; i <= n; i++) {
            pNext = pPrevPrev + pPrev;
            pPrevPrev = pPrev;
            pPrev = pCurr;
            pCurr = pNext;
        }

        return pNext;
    }
}
