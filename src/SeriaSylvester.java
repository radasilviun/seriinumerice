public class SeriaSylvester {

    public static void main(String[] args) {
        int n = 6;
        printSequence(n);

    }

    public static void printSequence(int n) {
        int a = 1;
        int ans = 2;
        int N = 1000000007;

        for (int i = 1; i <= n; i++) {
            System.out.print(ans + " ");
            ans = ((a % N) * (ans % N)) % N;
            a = ans;
            ans = (ans + 1) % N;
        }
    }


}
